#include<stdio.h>
struct fractions
{
    int num;
    int den;
};
typedef struct fractions f;
void input_n(int *n)
{
    printf("Enter the number of fractions-");
    scanf("%d",n);
}
f input_fraction()
{
    f f1;
    printf("Numerator-");
    scanf("%d",&f1.num);
    printf("Denominator-");
    scanf("%d",&f1.den);
    return f1;
}
void input_n_fractions(int n,f frac[n])
{
    for(int i=0;i<n;i++)
    {
        printf("Enter the %d fraction-\n",i+1);
        frac[i]=input_fraction();
    }
}
int lcm(int n,f frac[n])
{
    int l=1;
    for(int i=0;i<n;i++)
    {
        l=l*frac[i].den;
    }
    return l;
}
int numerator(int n,f frac[n])
{
    int n1,n2=1,n3=0;
    for(int i=0;i<n;i++)
    {
        n1=frac[i].num;              
        for(int j=0;j<n;j++)
        {
            if(j!=i)
            n2=n2 * frac[j].den;
        }
        n3=n3+(n1*n2);
        n2=1;
    }
    return n3;
}
void small(int *fn,int *fd)
{
    int gcd;
    for(int i=1;i<=*fn && i<=*fd;i++)
    {
        if(*fn%i==0 && *fd%i==0)
        {
            gcd=i;
        }
    }
    *fn=*fn/gcd;
    *fd=*fd/gcd;
}
void output(int n,f frac[n],int fn,int fd)
{
    printf("The sum of the fractions ");
    for(int i=0;i<n;i++)
    {
        printf("%d/%d ",frac[i].num,frac[i].den);
    }
    printf("=%d/%d\n",fn,fd);
}
int main()
{
    int n,fn,fd;
    input_n(&n);
    f frac[n];
    input_n_fractions(n,frac);
    fd=lcm(n,frac);
    fn=numerator(n,frac);
    small(&fn,&fd);
    output(n,frac,fn,fd);
}