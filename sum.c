#include<stdio.h>
void input(int *n)
{
	printf("Enter the number of array elements-");
	scanf("%d",n);
}
void input_array(int n,int a[n])
{
	int i;
	printf("Enter the elements-");
	for(i=0;i<n;i++)
	scanf("%d",&a[i]);
}
int find_sum(int n,int a[n])
{
	int sum=0;
	for(int i=0;i<n;i++)
	sum=sum+a[i];
	return sum;
}
void output(int n,int a[n])
{
    printf("The elements-");
    for(int i=0;i<n;i++)
	printf("%d ",a[i]);
    printf("\nThe sum of these elements=%d\n",find_sum(n,a));
}	
int main()
{
	int n;
	input(&n);
	int a[n];
	input_array(n,a);
	output(n,a);
	return 0;
}	