#include<stdio.h>
void input(int *n,int *d)
{
    printf("Enter the values of numerator and denominator-");
    scanf("%d %d",n,d);
}
int lcm(int d1,int d2)
{
    int l=d1*d2;
    return l;
}
int num(int n1,int d1,int n2,int d2)
{
    int n=(n1*d2)+(n2*d1);
    return n;
}
void small(int *n3,int *d3)
{
    int gcd;
    for(int i=1;i<=*n3 && i<=*d3;i++)
    {
        if(*n3%i==0 && *d3%i==0)
        gcd=i;
    }
    *n3=*n3/gcd;
    *d3=*d3/gcd;
}
void output(int n1,int d1,int n2,int d2,int n3,int d3)
{
    printf("The sum of the fractions of %d/%d+%d/%d=%d/%d",n1,d1,n2,d2,n3,d3);
}
int main()
{
    int n1,d1,n2,d2,n3,d3;
    input(&n1,&d1);
    input(&n2,&d2);
    d3=lcm(d1,d2);
    n3=num(n1,d1,n2,d2);
    small(&n3,&d3);
    output(n1,d1,n2,d2,n3,d3);
}