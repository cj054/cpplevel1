#include<stdio.h>
#include<math.h>
void input(float *x,float *y)
{
    printf("Enter the coordinates of a point-");
    scanf("%f %f",x,y);
}
float find_distance(float x1,float y1,float x2,float y2)
{
    float d=sqrt(pow((x2-x1),2)+pow((y2-y1),2));
    return d;
}
int output(float d)
{
    printf("The distance between two points=%f",d);
    return 0;
}
int main()
{
    float x1,y1,x2,y2,d;
    input(&x1,&y1);
    input(&x2,&y2);
    d=find_distance(x1,y1,x2,y2);
    output(d);
    return 0;
}    